const TransferController                         = require('./Controllers/TransferController');
const SendController                             = require('./Controllers/SendController');

const Joi               = require('joi');

module.exports = [
{
    method: 'POST',
    path: '/sendmoney',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: 'SuperAdmin'
        },
        handler: SendController.SendMoney,
        validate: {
            payload: {
                sessiontoken : Joi.string().min(20).max(20),
                dstCardN : Joi.string().min(16).max(16),
                amount : Joi.number().min(0),
                livemode: Joi.string().min(4).max(5),
                description : Joi.string(),
                ipaddress : Joi.string(),
            }
        }
    }
},
]