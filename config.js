const STRIPETESTKEY                 = process.env.STRIPETESTKEY;
const STRIPELIVEKEY                 = process.env.STRIPELIVEKEY;

module.exports = {
    Server: {
        Port: 3015
    },
    manager : {
        superAdmin: {
            scope: ['SuperAdmin'],
            superManagerToken: 'send-access-token'
        }
    },
    vendor : {
        url : 'http://stagingapivendor.ekumbu.com'
    },
    deposit: {
        depositTokenID : "435324dsfsd32423SFDSFFDS23432423DSXF32432SDSDWSADSADasdasr32423sdadSDFDSF",
        trackingID : "6adkA453kuA4wydhsadB4dfasj2SDFdcsdkd34jadsgAWGDJV43422scsd2234cndLPCmxapzqDSF453weasdDAd",
        fee: 6
    },
    stripe : {
        TESTSKEY : STRIPETESTKEY || 'sk_test_AzVP5gfs2tLNZAjt5ERvGZ31',
        LIVESKEY : STRIPELIVEKEY || 'sk_live_uK6XxRZqb20FAWNU95b6IaOK' 
    },
    mail: {
        ekumbumail : "mail.ekumbu.com",
        mailkey : "key-6c9a2780e8f0a9a0976bd6938e0ffb23"
    },
    transfer : {
        transferID : '89313920398183290834398539453902443754309732483289324',
        trackingID: '6adkA453kuA4wydhsadB4dfasj2SDFdcsdkd34jadsgAWGDJV43422scsd2234cndLPCmxapzqDSF453weasdDAd'
    },
    send: {
        sendID: '34590099382371494920482958295829592945812011109385726581942',
        trackingID: '6adkA453kuA4wydhsadB4dfasj2SDFdcsdkd34jadsgAWGDJV43422scsd2234cndLPCmxapzqDSF453weasdDAd'
    }
}