const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
var db                      = require('../MongoManager/MongoConnection');
const bcrypt                = require('bcryptjs');

const CustomerEventSchema = new Schema({
    created : { type: Date, default: Date.now },
    type: {type: String, require: true},
    amount: {type: String, require: true},
    customer_id : {type : String, required: true},
    hour: {type: String},
    date: {type: String},
    source_info : {
        name: {type: String},
        sourceid: {type: String}
    },
    details : {
        id : { type : String, require: true }
    }
});

var customere = Mongoose.model('customerevents', CustomerEventSchema);
module.exports = {
    CustomerEventSchema: customere
};