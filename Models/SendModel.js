const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
const db                    = require('../MongoManager/MongoConnection');
const bcrypt                = require('bcryptjs');

const SendSchema = new Schema({
    created : { type: Date, default: Date.now },
    status : { type : String, required: true },
    sendID: { type: String, required: true, unique: true },
    amountToSend: { type : Number, required: true },
    type: {type: String, required: true},
    amountToReceive: { type : Number, required: true },
    description : { type: String },
    trackingID: {type: String, required: true},
    senderInfo : {
        tokenID: { type: String, required: true },
        cardID: { type: String, required: true },
    },
    receiverInfo : {
        tokenID: { type: String, required: true },
        cardID: { type: String, required: true },
    }
});

var send = Mongoose.model('sends', SendSchema);
module.exports = {
    SendSchema: send
};