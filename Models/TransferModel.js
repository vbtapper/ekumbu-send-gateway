const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
var db                      = require('../MongoManager/MongoConnection');
const bcrypt                = require('bcryptjs');

const TransferSchema = new Schema({
    created : { type: Date, default: Date.now },
    status : { type : String, required: true },
    transferID: { type: String, required: true, unique: true },
    originalAmount: { type : Number, required: true },
    amountToTransfer: { type : Number, required: true },
    amountWFee: { type : Number, required: true },
    fee : { type: String },
    rate : {type: String},
    description : { type: String },
    trackingID: {type: String, required: true},
    senderInfo : {
        tokenID: { type: String, required: true },
        lastCardNmber: { type: String, required: true },
        ekbExpMonth: { type: String, required: true },
        ekbExpYear: { type: String, required: true },
        country: { type: String, default: 'Angola', required: true },
        currency : { type: String, default: 'AOA', required: true },
    },
    receiverInfo : {
        destinationNumber: { type: String, required: true },
        country: { type: String, required: true },
        currency : { type: String, required: true },
        name : { type: String, required: true },
        institutionName : { type: String, required: true },
        statementDescription : { type: String, required: true },
        tokenID: { type: String, required: true },
    },
    attachement: {
        fileID : {type: String, default: 'none', required: true}
    },
    statusInfo: {
        message : {type: String, default: 'none', required: true}
    }
});

var transfer = Mongoose.model('transfers', TransferSchema);
module.exports = {
    TransferSchema: transfer
};