const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
var db                      = require('../MongoManager/MongoConnection');
const bcrypt                = require('bcryptjs');

SALT_WORK_FACTOR = 101;

const CustomerSchema = new Schema({
    created: { type: Date, default: Date.now },
    status : {type : String, default: "false"},
    description : { type: String, default: "none" },
    email: { type: String, unique: true, required: true },
    firstname: { type: String, required: true },
    scope: [{type: String}],
    lastname: { type: String, required: true },
    phone: { type: String, required: true},
    nfailedlogins : { type: Number, required: true, default: 0},
    source : {
        object : String,
        data : [{
            key: String,
            value: String
        }],
        has_more: Boolean,
        total_count: Number,
        url : String
    },
    security: {
        active : { type: Boolean, default: true },
        last_token: { type: String, default: "000000", required: true },
        token_expire : { type: Boolean, default: false, required: true },
        token_created : { type: Date, default: Date.now }
    },
    password: { type: String, required: true },
    sessiontoken: { type: String },
    sessionstatus: {type: Boolean, default: false}
});

CustomerSchema.pre('save', function(next) {
    //console.log('pre save');
    var customer = this;
    // only hash the password if it has been modified (or is new)
    if (!customer.isModified('password')) return next();
    //console.log('before hash');
    bcrypt.hash(customer.password, SALT_WORK_FACTOR, function(err, hash) {
        //console.log(err);
        if (err) return next(err);
        // override the cleartext password with the hashed one
        customer.password = hash;
        next();
    });
});

CustomerSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

var customer = Mongoose.model('customer', CustomerSchema);
module.exports = {
    CustomerSchema: customer
};