
const randtoken                   = require('rand-token').generator();
const moment                      = require('moment');
const formatCurrency              = require('format-currency')

const config                      = require('../config');
const Q                           = require('q');

const Client                      = require('node-rest-client').Client;

const mailgun                     = require('mailgun-js')({ apiKey: config.mail.mailkey, domain: config.mail.ekumbumail });
const Customer                    = require('../Models/CustomerModel').CustomerSchema;      
const Card                        = require('../Models/CardModel').CardSchema; 
const CustomEvent                 = require('../Models/CustomerEventModel').CustomerEventSchema;
const TransferModel               = require('../Models/TransferModel').TransferSchema;
const Token                       = require('../Models/TokenModel').TokenSchema;


function GetCustomerBySessionToken(token) {
    var deferred = Q.defer();
    Customer.findOne({"sessiontoken": token}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null);
            }
            else {
                deferred.resolve(data);
            }
         }
    });
    return deferred.promise;
}

function GetCardByCardNumber(cardNumber) {
    var deferred = Q.defer();
    Card.findOne({"number": cardNumber}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(err);
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function GetCustomerByID(customerID) {
    var deferred = Q.defer();
    Customer.findOne({"_id": customerID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function GetCardByCustomerID(cutomerID) {
    var deferred = Q.defer();
    Card.findOne({"cardholder_info.customer_id": cutomerID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function CreateTokenTransfer(CustID, Card, mode, ipAddress) {
    var deferred = Q.defer();

    var newTokenInfo = {
        type : 'Transfer',
        livemode : mode,
        customer_ip : ipAddress,
        card : Card._id,
        source_info: {
            source_id : CustID
        }
    }
    
    var newToken = new Token(newTokenInfo)
    newToken.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

function ChargeEkumbuAccount(tokenp, amount) {

    var deferred = Q.defer();

    var client = new Client();
    var args = {
        headers: { 
            "Content-Type": "application/json",
        },
        data: {
            token : tokenp,
            amount: amount.toString()
        },
    };
    client.post(config.vendor.url + '/charge', args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data);
        } 
        else {
            deferred.reject(data);
        }
    });
    
    return deferred.promise;
}

function MakeEkumbuDeposit(cardToken, amountp) {
    var deferred = Q.defer();

    var client = new Client();
    var args = {
        data : {
            token : cardToken,
            amount : amountp
        },
        headers: { 
            "Content-Type": "application/json",
        }
    };
    client.post(config.vendor.url + '/deposit', args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data);
        }
        else {
            deferred.reject(data['message']);
        }
    });

    return deferred.promise;
}

function CreateSendMoneyTransfer(originalAmountParam, feeParam, descriptionParam, rateParam, senderEkumbuCard, senderTokenID, receiverDNumber, receiverName, receiverTokenID) {
    var deferred = Q.defer();

    var newTransferInfo = {
        status : 'Processing',
        transferID: "trf_"+randtoken.generate(20, config.transfer.transferID),
        originalAmount: originalAmountParam,
        amountToTransfer : originalAmountParam,
        amountWFee : originalAmountParam,
        fee : feeParam,
        rate : rateParam,
        description : descriptionParam,
        trackingID : randtoken.generate(20, config.transfer.trackingID),
        senderInfo : {
            tokenID: senderTokenID,
            lastCardNmber: senderEkumbuCard.number,
            ekbExpMonth: senderEkumbuCard.exp_month,
            ekbExpYear: senderEkumbuCard.exp_year
        },
        receiverInfo : {
            destinationNumber: receiverDNumber,
            country: 'Angola',
            currency : 'AOA',
            name : receiverName,
            institutionName : 'Ekumbu',
            statementDescription : descriptionParam,
            tokenID: receiverTokenID,
        }
    }

    var newTransf = new TransferModel(newTransferInfo) 
    newTransf.save(function(err, data) {
        if(err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

function CreateCustomerEventTransfer(customerID, amountA, destinationName, transfID, destinationID) {
    var deferred = Q.defer();

    Date.prototype.monthNames = [
        "Janeiro", "Fevereiro", "Março",
        "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro",
        "Octubro", "Novembro", "Dezembro"
    ];

    Date.prototype.getMonthName = function() {
        return this.monthNames[this.getMonth()];
    };
    Date.prototype.getShortMonthName = function () {
        return this.getMonthName().substr(0, 3);
    };
    var dt = new Date();

    var hours = dt.getHours();
    var mins = dt.getMinutes();
    var day = dt.getDate();
    var month = dt.getShortMonthName();

    var newCeventInfo = {
        type: "transfer",
        amount: amountA,
        customer_id: customerID,
        hour: hours + ":" + mins,
        date: month + " " + day,
        source_info : {
            name: destinationName,
            sourceid: destinationID
        },
        details : {
            id : transfID
        }
    }

    var newCustEvt = new CustomEvent(newCeventInfo)
    newCustEvt.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        deferred.resolve(data)
    });

    return deferred.promise;
}


module.exports = {
    Transfer: function(request, reply) {
    }
}