
const randtoken                   = require('rand-token').generator();
const moment                      = require('moment');
const formatCurrency              = require('format-currency')

const config                      = require('../config');
const Q                           = require('q');

const Client                      = require('node-rest-client').Client;

const mailgun                     = require('mailgun-js')({ apiKey: config.mail.mailkey, domain: config.mail.ekumbumail });
const Customer                    = require('../Models/CustomerModel').CustomerSchema;      
const Card                        = require('../Models/CardModel').CardSchema; 
const CustomEvent                 = require('../Models/CustomerEventModel').CustomerEventSchema;
const SendModel                   = require('../Models/SendModel').SendSchema;
const Token                       = require('../Models/TokenModel').TokenSchema;


function GetCustomerBySessionToken(token) {
    var deferred = Q.defer();
    Customer.findOne({"sessiontoken": token}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null);
            }
            else {
                deferred.resolve(data);
            }
         }
    });
    return deferred.promise;
}

function GetCardByCardNumber(cardNumber) {
    var deferred = Q.defer();
    Card.findOne({"number": cardNumber}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(err);
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function GetCustomerByID(customerID) {
    var deferred = Q.defer();
    Customer.findOne({"_id": customerID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function GetCardByCustomerID(cutomerID) {
    var deferred = Q.defer();
    Card.findOne({"cardholder_info.customer_id": cutomerID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function CreateTokenTransfer(CustID, Card, mode, ipAddress) {
    var deferred = Q.defer();

    var newTokenInfo = {
        type : 'Transfer',
        livemode : mode,
        customer_ip : ipAddress,
        card : Card._id,
        source_info: {
            source_id : CustID
        }
    }
    
    var newToken = new Token(newTokenInfo)
    newToken.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

function ChargeEkumbuAccount(tokenp, amount) {

    var deferred = Q.defer();

    var client = new Client();
    var args = {
        headers: { 
            "Content-Type": "application/json",
        },
        data: {
            token : tokenp,
            amount: amount.toString()
        },
    };
    client.post(config.vendor.url + '/charge', args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data);
        } 
        else {
            deferred.reject(data);
        }
    });
    
    return deferred.promise;
}

function MakeEkumbuDeposit(cardToken, amountp) {
    var deferred = Q.defer();

    var client = new Client();
    var args = {
        data : {
            token : cardToken,
            amount : amountp
        },
        headers: { 
            "Content-Type": "application/json",
        }
    };
    client.post(config.vendor.url + '/deposit', args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data);
        }
        else {
            deferred.reject(data['message']);
        }
    });

    return deferred.promise;
}

function CreateSendMoneyTransfer(originalAmountParam, typeParam, descriptionParam, senderEkumbuCard, senderTokenID, receiverEkumbuCard, receiverTokenID) {
    var deferred = Q.defer();

    var newTransferInfo = {

        status : 'Processing',
        sendID: "snd_"+randtoken.generate(20, config.send.sendID),
        amountToSend: originalAmountParam,
        type: typeParam,
        amountToReceive: originalAmountParam,
        description : descriptionParam,
        trackingID: randtoken.generate(20, config.send.trackingID),
        senderInfo : {
            tokenID: senderTokenID,
            cardID: senderEkumbuCard._id,
        },
        receiverInfo : {
            tokenID: receiverTokenID,
            cardID: receiverEkumbuCard._id,
        }
    }

    var newTransf = new SendModel(newTransferInfo) 
    newTransf.save(function(err, data) {
        if(err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

function CreateCustomerEventTransfer(customerID, amountA, destinationName, transfID, destinationID) {
    var deferred = Q.defer();

    Date.prototype.monthNames = [
        "Janeiro", "Fevereiro", "Março",
        "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro",
        "Octubro", "Novembro", "Dezembro"
    ];

    Date.prototype.getMonthName = function() {
        return this.monthNames[this.getMonth()];
    };
    Date.prototype.getShortMonthName = function () {
        return this.getMonthName().substr(0, 3);
    };
    var dt = new Date();

    var hours = dt.getHours();
    var mins = dt.getMinutes();
    var day = dt.getDate();
    var month = dt.getShortMonthName();

    var newCeventInfo = {
        type: "send",
        amount: amountA,
        customer_id: customerID,
        hour: hours + ":" + mins,
        date: month + " " + day,
        source_info : {
            name: destinationName,
            sourceid: destinationID
        },
        details : {
            id : transfID
        }
    }

    var newCustEvt = new CustomEvent(newCeventInfo)
    newCustEvt.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        deferred.resolve(data)
    });

    return deferred.promise;
}


module.exports = {
    SendMoney: function(request, reply) {
        GetCustomerBySessionToken(request.payload.sessiontoken)
        .then(SenderCustomer => {
            GetCardByCardNumber(request.payload.dstCardN)
            .then(ReceiverCard => {
                GetCustomerByID(ReceiverCard.cardholder_info.customer_id)
                .then(ReceiverCustomer => {
                    GetCardByCustomerID(SenderCustomer._id)
                    .then(SenderCard => {
                        CreateTokenTransfer(SenderCustomer._id, SenderCard, request.payload.livemode, request.payload.ipaddress)
                        .then(SenderToken => {
                            CreateTokenTransfer(ReceiverCustomer._id, ReceiverCard, request.payload.livemode, request.payload.ipaddress)
                            .then(ReceiverToken => {
                                ChargeEkumbuAccount(SenderCard.security.secret_token, request.payload.amount.toString())
                                .then(EkumbuCharge => {
                                    MakeEkumbuDeposit(ReceiverCard.security.secret_token, request.payload.amount.toString())
                                    .then(EkumbuDeposit => {
                                        CreateSendMoneyTransfer(request.payload.amount, 'sender', request.payload.description, SenderCard, SenderToken._id, ReceiverCard, ReceiverToken._id)
                                        .then(SenderTransferCreated => {
                                            CreateSendMoneyTransfer(request.payload.amount, 'receiver', request.payload.description, SenderCard, SenderToken._id, ReceiverCard, ReceiverToken._id)
                                            .then(ReceiverTransferCreated => {
                                                // Sender Customer Event
                                                let opts = {
                                                    format : '%v %c', 
                                                    code : 'AOA'
                                                }
                                                var formatedAmount = formatCurrency(request.payload.amount, opts)
                                                var receiverName = ReceiverCustomer.firstname + " " + ReceiverCustomer.lastname
                                                CreateCustomerEventTransfer(SenderCustomer._id, formatedAmount, receiverName, SenderTransferCreated._id, ReceiverCustomer._id)
                                                .then(SenderCustomerEvent => {

                                                    sdcNumber = SenderCard.number.toString();
                                                    var destName = "Ekumbu x-" + sdcNumber.substr(sdcNumber.toString().length-4, sdcNumber.toString().length);
                                                    
                                                    // Receiver Customer Event
                                                    CreateCustomerEventTransfer(ReceiverCustomer._id, formatedAmount, destName, ReceiverTransferCreated._id, ReceiverCard._id)
                                                    .then(ReceiverCustomerEvent => {
                                                        var rspInfo = {
                                                            statusCode: 200,
                                                            error: null,
                                                            message: 'success',
                                                            trackingID: SenderTransferCreated.trackingID
                                                        } 
                                                        return reply(rspInfo);
                                                    })
                                                    .catch(err => {
                                                        console.log(err)
                                                        var rspInfo = {
                                                            statusCode: 500,
                                                            error: 'Internal Error',
                                                            message: 'Problems creating sender customer event ' + err
                                                        }
                                                        return reply(rspInfo)
                                                    })
                                                })
                                                .catch(err => {
                                                    console.log(err)
                                                    var rspInfo = {
                                                        statusCode: 500,
                                                        error: 'Internal Error',
                                                        message: 'Problems creating sender customer event ' + err
                                                    }
                                                    return reply(rspInfo)
                                                })
                                            }).catch(error => {
                                                console.log(err)
                                                var rspInfo = {
                                                    statusCode: 500,
                                                    error: 'Internal Error',
                                                    message: 'Problems creating receiver transfer ' + err
                                                }
                                                return reply(rspInfo)
                                            })  
                                        })
                                        .catch(err => {
                                            console.log(err)
                                            var rspInfo = {
                                                statusCode: 500,
                                                error: 'Internal Error',
                                                message: 'Problems creating sender transfer ' + err
                                            }
                                            return reply(rspInfo)
                                        })
                                    })
                                    .catch(err => {
                                        console.log(err)
                                        if (err.message.toString() === 'Account does not exist') {
                                            var rspInfo = {
                                                statusCode: 403,
                                                error: 'Request Error',
                                                message: 'Problems finding receiver information at vendor API ' + err
                                            }
                                            return reply(rspInfo)
                                        }
                                        else {
                                            var rspInfo = {
                                                statusCode: 500,
                                                error: 'Internal Error',
                                                message: 'An internal error occured on ekumbu vendor api' + err
                                            }
                                            return reply(rspInfo)
                                        }    
                                    })
                                })
                                .catch(err => {
                                    console.log(err)
                                    if(err.message.toString() === 'declined') {
                                        var rspInfo = {
                                            statusCode: 403,
                                            error: 'Request Error',
                                            message: 'declined' + err
                                        }
                                        return reply(rspInfo)
                                    }
                                    else if (err.message.toString() === 'Account does not exist') {
                                        var rspInfo = {
                                            statusCode: 403,
                                            error: 'Request Error',
                                            message: 'Problems finding sender information at vendor API ' + err
                                        }
                                        return reply(rspInfo)
                                    }
                                    else {
                                        var rspInfo = {
                                            statusCode: 500,
                                            error: 'Internal Error',
                                            message: 'An internal error occured on ekumbu vendor api' + err
                                        }
                                        return reply(rspInfo)
                                    }    
                                })
                            })
                            .catch(err => {
                                console.log(err)
                                var rspInfo = {
                                    statusCode: 500,
                                    error: 'Internal Error',
                                    message: 'Problems creating receiver token ' + err
                                }
                                return reply(rspInfo)
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            var rspInfo = {
                                statusCode: 500,
                                error: 'Internal Error',
                                message: 'Problems creating sender token ' + err
                            }
                            return reply(rspInfo)
                        })
                    })
                    .catch(err => {
                        console.log(err)
                        var rspInfo = {
                            statusCode: 404,
                            error: 'Not Found',
                            message: 'Problems finding sender card ' + err
                        }
                        return reply(rspInfo)
                    })
                })
                .catch(err => {
                    console.log(err)
                    var rspInfo = {
                        statusCode: 404,
                        error: 'Not Found',
                        message: 'Problems finding receiver customer ' + err
                    }
                    return reply(rspInfo)
                })
            })
            .catch(err => {
                console.log(err)
                var rspInfo = {
                    statusCode: 404,
                    error: 'Not Found',
                    message: 'Problems finding receiver card ' + err
                }
                return reply(rspInfo)
            })
        })
        .catch(err => {
            console.log(err)
            var rspInfo = {
                statusCode: 404,
                error: 'Not Found',
                message: 'Problems finding sender information ' + err
            }
            return reply(rspInfo)
        })
    }
}